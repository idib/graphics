package Graphic.Comp;

import javafx.geometry.Point2D;

/**
 * Created by idib on 05.09.17.
 */
public class Edge {
    private Point first, second;

    public Edge(Point first, Point second) {
        this.first = first;
        this.second = second;
    }

    public Point2D getFirst(Matrix trans) {
        Matrix mat = Matrix.mul(first.getMatrix(), trans);
        return new Point2D(mat.getMat(0, 0), mat.getMat(0, 1));
    }

    public Point2D getSecond(Matrix trans) {
        Matrix mat = Matrix.mul(second.getMatrix(), trans);
        return new Point2D(mat.getMat(0, 0), mat.getMat(0, 1));
    }

}
