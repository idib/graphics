package Graphic.Comp;

/**
 * Created by idib on 12.09.17.
 */
public class Point {
    public double x, y, z;

    public Point(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point(Point t) {
        x = t.x;
        y = t.y;
        z = t.z;
    }

    public Matrix getMatrix() {
        return new Matrix(new double[][]{{x, y, z, 1}});
    }

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == Point.class) {
            Point t = (Point)obj;
            return x == t.x && y == t.y && z == t.z;
        }
        return super.equals(obj);
    }
}
