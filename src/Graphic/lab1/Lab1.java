package Graphic.lab1;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

import java.io.IOException;

public class Lab1 extends Application {
    private WritableImage dest;
    private int width;
    private int height;


    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("lab1.fxml"));
        Scene scene = new Scene(root);

        stage.setTitle("Lab1");
        stage.setScene(scene);
        stage.show();
    }
//
//    private void initImage(AnchorPane root) {
//
//
//        ImageView srcView = new ImageView(src);
//        root.getChildren().add(srcView);
//        AnchorPane.setTopAnchor(srcView, 0.0);
//        AnchorPane.setLeftAnchor(srcView, 0.0);
//
//        width = (int) src.getWidth();
//        height = (int) src.getHeight();
//        root.setPrefSize(width * 2.0, height + 50);
//
//        dest = new WritableImage(width, height);
//        ImageView destView = new ImageView(dest);
//        destView.setTranslateX(width);
//        root.getChildren().add(destView);
//        AnchorPane.setTopAnchor(destView, 0.0);
//        AnchorPane.setRightAnchor(destView, (double) width);
//
//        Slider slider = new Slider(0, 10, kernelSize);
//        slider.setPrefSize(width, 50);
//        slider.setShowTickLabels(true);
//        slider.setShowTickMarks(true);
//        slider.setSnapToTicks(true);
//        slider.setMajorTickUnit(1.0);
//        slider.setMinorTickCount(0);
//
//        slider.valueProperty().addListener(new InvalidationListener() {
//            @Override
//            public void invalidated(Observable o) {
//                DoubleProperty value = (DoubleProperty) o;
//                int intValue = (int) value.get();
//                if (intValue != kernelSize) {
//                    kernelSize = intValue;
//                    if (blurButton.isSelected()) {
//                        blur();
//                    } else if (blur2Button.isSelected()) {
//                        blur2();
//                    } else {
//                        mosaic();
//                    }
//                }
//            }
//        });
//
//        root.getChildren().add(slider);
//        AnchorPane.setBottomAnchor(slider, 0.0);
//        AnchorPane.setRightAnchor(slider, 10.0);
//
//        HBox hbox = new HBox(10);
//        hbox.setAlignment(Pos.CENTER);
//        hbox.setPrefWidth(width);
//        hbox.setPrefHeight(50);
//        root.getChildren().add(hbox);
//        AnchorPane.setBottomAnchor(hbox, 0.0);
//        AnchorPane.setLeftAnchor(hbox, 10.0);
//
//        ToggleGroup group = new ToggleGroup();
//        blurButton = new RadioButton("Blur");
//        blurButton.setToggleGroup(group);
//        blurButton.setSelected(true);
//        hbox.getChildren().add(blurButton);
//        blur2Button = new RadioButton("Blur2");
//        blur2Button.setToggleGroup(group);
//        hbox.getChildren().add(blur2Button);
//        mosaicButton = new RadioButton("Mosaic");
//        mosaicButton.setToggleGroup(group);
//        hbox.getChildren().add(mosaicButton);
//
//        blur();
//    }
//
//    private void blur() {
//        PixelReader reader = src.getPixelReader();
//        PixelWriter writer = dest.getPixelWriter();
//
//        for (int x = 0; x < width; x++) {
//            for (int y = 0; y < height; y++) {
//                double red = 0;
//                double green = 0;
//                double blue = 0;
//                double alpha = 0;
//                int count = 0;
//                for (int i = -kernelSize; i <= kernelSize; i++) {
//                    for (int j = -kernelSize; j <= kernelSize; j++) {
//                        if (x + i < 0 || x + i >= width
//                                || y + j < 0 || y + j >= height) {
//                            continue;
//                        }
//                        Color color = reader.getColor(x + i, y + j);
//                        red += color.getRed();
//                        green += color.getGreen();
//                        blue += color.getBlue();
//                        alpha += color.getOpacity();
//                        count++;
//                    }
//                }
//                Color blurColor = Color.color(red / count,
//                        green / count,
//                        blue / count,
//                        alpha / count);
//                writer.setColor(x, y, blurColor);
//            }
//        }
//    }
//
//    private void blur2() {
//        PixelReader reader = src.getPixelReader();
//        PixelWriter writer = dest.getPixelWriter();
//        WritablePixelFormat<IntBuffer> format
//                = WritablePixelFormat.getIntArgbInstance();
//
//        for (int x = 0; x < width; x++) {
//            for (int y = 0; y < height; y++) {
//                int centerX = x - kernelSize;
//                int centerY = y - kernelSize;
//                int kernelWidth = kernelSize * 2 + 1;
//                int kernelHeight = kernelSize * 2 + 1;
//
//                if (centerX < 0) {
//                    centerX = 0;
//                    kernelWidth = x + kernelSize;
//                } else if (x + kernelSize >= width) {
//                    kernelWidth = width - centerX;
//                }
//
//                if (centerY < 0) {
//                    centerY = 0;
//                    kernelHeight = y + kernelSize;
//                } else if (y + kernelSize >= height) {
//                    kernelHeight = height - centerY;
//                }
//
//                int[] buffer = new int[kernelWidth * kernelHeight];
//                reader.getPixels(centerX, centerY,
//                        kernelWidth, kernelHeight,
//                        format, buffer, 0, kernelWidth);
//
//                int alpha = 0;
//                int red = 0;
//                int green = 0;
//                int blue = 0;
//
//                for (int color : buffer) {
//                    alpha += (color >>> 24);
//                    red += (color >>> 16 & 0xFF);
//                    green += (color >>> 8 & 0xFF);
//                    blue += (color & 0xFF);
//                }
//                alpha = alpha / kernelWidth / kernelHeight;
//                red = red / kernelWidth / kernelHeight;
//                green = green / kernelWidth / kernelHeight;
//                blue = blue / kernelWidth / kernelHeight;
//
//                int blurColor = (alpha << 24)
//                        + (red << 16)
//                        + (green << 8)
//                        + blue;
//                writer.setArgb(x, y, blurColor);
//            }
//        }
//    }
//
//    private void mosaic() {
//        PixelReader reader = src.getPixelReader();
//        PixelWriter writer = dest.getPixelWriter();
//        WritablePixelFormat<IntBuffer> format
//                = WritablePixelFormat.getIntArgbInstance();
//
//        for (int x = kernelSize; x < width - kernelSize * 2; x += kernelSize * 2 + 1) {
//            for (int y = kernelSize; y < height - kernelSize * 2; y += kernelSize * 2 + 1) {
//                int kernelWidth = kernelSize * 2 + 1;
//                int kernelHeight = kernelSize * 2 + 1;
//
//                int[] buffer = new int[kernelWidth * kernelHeight];
//                reader.getPixels(x, y,
//                        kernelWidth, kernelHeight,
//                        format, buffer, 0, kernelWidth);
//
//                int alpha = 0;
//                int red = 0;
//                int green = 0;
//                int blue = 0;
//
//                for (int color : buffer) {
//                    alpha += (color >>> 24);
//                    red += (color >>> 16 & 0xFF);
//                    green += (color >>> 8 & 0xFF);
//                    blue += (color & 0xFF);
//                }
//                alpha = alpha / kernelWidth / kernelHeight;
//                red = red / kernelWidth / kernelHeight;
//                green = green / kernelWidth / kernelHeight;
//                blue = blue / kernelWidth / kernelHeight;
//
//                int blurColor = (alpha << 24)
//                        + (red << 16)
//                        + (green << 8)
//                        + blue;
//                Arrays.fill(buffer, blurColor);
//                writer.setPixels(x, y,
//                        kernelWidth, kernelHeight,
//                        format, buffer, 0, kernelWidth);
//            }
//        }
//    }

    public static void main(String[] args) {
        launch(args);
    }
}