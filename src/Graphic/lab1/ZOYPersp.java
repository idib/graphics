package Graphic.lab1;

import Graphic.Comp.Matrix;

/**
 * Created by idib on 18.09.17.
 */
public class ZOYPersp implements persp {
    Matrix cur = new Matrix(new double[][]{
            {0, 0, 0, 0},
            {0, 1, 0, 0},
            {1, 0, 0, 0},
            {0, 0, 0, 1}

    });

    @Override
    public Matrix get() {
        return cur;
    }
}
