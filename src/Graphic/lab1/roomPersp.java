package Graphic.lab1;

import Graphic.Comp.Matrix;

/**
 * Created by idib on 18.09.17.
 */
public class roomPersp implements persp {
    double t = Math.cos(Math.PI / 4) / 2;
    Matrix cur = new Matrix(new double[][]{
            {1, 0, 0, 0},
            {0, 1, 0, 0},
            {t, t, 0, 0},
            {0, 0, 0, 1}});

    @Override
    public Matrix get() {
        return cur;
    }
}
