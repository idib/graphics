package Graphic.lab2;

import Graphic.Comp.Edge;
import Graphic.Comp.Matrix;
import Graphic.Comp.Point;
import Graphic.lab1.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.*;

import static java.lang.Math.cos;
import static java.lang.Math.sin;

/**
 * Created by idib on 05.09.17.
 */
public class Controller {
	public ToggleGroup perspect;
	public Canvas CanvasView;
	public Canvas OSView;
	int con = 30;
	double scale;
	private List<Edge> edges;
	private List<Edge> os;
	private int phi;
	private int hi;
	private int psi;
	private int ugol = 50;
	private double translateX, translateY, translateZ;
	private double scaleX = 1, scaleY = 1, scaleZ = 1;
	private double px = 0, py = 0, pz = 0;
	private Point curPoint;
	private List<Point> pointSet = new ArrayList<>();
	private Timer tim = new Timer();
	private Random r = new Random();
	private boolean t = false;
	private boolean revers = false;
	private Point vec;
	private int time = 0;
	private int maxT = 10;
	private boolean newLoop = true;
	private boolean rr = true;
	private Point clonePoint;
	private persp CurPersp = new roomPersp();

	private void initOS() {
		os = new ArrayList<>();
		Point nul = new Point(0, 0, 0);
		os.add(new Edge(nul, new Point(1, 0, 0)));
		os.add(new Edge(nul, new Point(0, 1, 0)));
		os.add(new Edge(nul, new Point(0, 0, 1)));
	}

	private void intitCOf() {
		phi = psi = hi = 0;
		translateX = translateY = translateZ = 0;
		scaleX = scaleY = scaleZ = 1;
	}

	@FXML
	private void refresh() {
		Matrix rotOs = CurPersp.get();
		Matrix rot = Matrix.mul(rotateOsX(phi * 1. / ugol), rotateOsZ(psi * 1. / ugol), rotateOsY(hi * 1. / ugol));
		Matrix mat = Matrix.mul(rot, translateXYZ(), scaleXYZ(), rotOs);

		GraphicsContext gc = CanvasView.getGraphicsContext2D();
		CanvasView.setHeight(scale);
		CanvasView.setWidth(scale);
		double tscale = scale / 2;
		gc.setFill(Color.GREEN);
		gc.setStroke(Color.BLUE);
		gc.setLineWidth(1);
		gc.clearRect(0, 0, CanvasView.getWidth(), CanvasView.getHeight());
		for (Edge edge : edges) {
			Point2D f = edge.getFirst(mat);
			Point2D s = edge.getSecond(mat);
			gc.strokeLine(f.getX() * con + tscale, f.getY() * con + tscale, s.getX() * con + tscale, s.getY() * con + tscale);
		}

		refreshOS(rotOs);
	}

	private void refreshOS(Matrix rot) {
		int con = 30;
		OSView.setHeight(4 * con);
		OSView.setWidth(4 * con);
		GraphicsContext gc = OSView.getGraphicsContext2D();
		gc.setFill(Color.GREEN);
		gc.setStroke(Color.BLUE);
		gc.setLineWidth(1);
		gc.clearRect(0, 0, OSView.getWidth(), OSView.getHeight());
		for (int i = 0; i < 3; i++) {
			Point2D f = os.get(i).getFirst(rot);
			Point2D s = os.get(i).getSecond(rot);
			gc.strokeLine(f.getX() * con + 1.5 * con, f.getY() * con + 1.5 * con, s.getX() * con + 1.5 * con, s.getY() * con + 1.5 * con);
			String str = "";
			switch (i) {
				case 0:
					str = "x";
					break;
				case 1:
					str = "y";
					break;
				case 2:
					str = "z";
					break;
			}
			gc.fillText(str, s.getX() * con + 1.5 * con, s.getY() * con + 1.5 * con);
		}
	}

	private Matrix rotateOsX(double phi) {
		return new Matrix(new double[][]{
				{1, 0, 0, 0},
				{0, cos(phi), sin(phi), 0},
				{0, -sin(phi), cos(phi), 0},
				{0, 0, 0, 1}});
	}

	private Matrix rotateOsZ(double psi) {
		return new Matrix(new double[][]{
				{cos(psi), 0, -sin(psi), 0},
				{0, 1, 0, 0},
				{sin(psi), 0, cos(psi), 0},
				{0, 0, 0, 1}});
	}

	private Matrix rotateOsY(double hi) {
		return new Matrix(new double[][]{
				{cos(hi), sin(hi), 0, 0},
				{-sin(hi), cos(hi), 0, 0},
				{0, 0, 1, 0},
				{0, 0, 0, 1}});
	}

	private Matrix translateXYZ() {
		return new Matrix(new double[][]{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {translateX, translateY, translateZ, 1}});
	}

	private Matrix scaleXYZ() {
		return new Matrix(new double[][]{{scaleX, 0, 0, 0}, {0, scaleY, 0, 0}, {0, 0, scaleZ, 0}, {0, 0, 0, 1}});
	}

	private Matrix perspectiveZ() {
		double x, y, z;
		if (px != 0)
			x = 1 / px;
		else
			x = 1;
		if (py != 0)
			y = 1 / py;
		else
			y = 1;
		if (pz != 0)
			z = 1 / pz;
		else
			z = 1;
		return new Matrix(new double[][]{{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}});
//        return new Matrix(new double[][]{{0, 0, 0, -x}, {0, 0, 0, -y}, {0, 0, 0, -z}, {0, 0, 0, 1}});
	}

	private void initTog() {
		perspect.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
			if (perspect.getSelectedToggle() != null) {

				System.out.println(perspect.getSelectedToggle().getUserData().toString());
				switch (perspect.getSelectedToggle().getUserData().toString()) {
					case "room":
						CurPersp = new roomPersp();
						break;
					case "XOY":
						CurPersp = new XOYPersp();
						break;
					case "ZOY":
						CurPersp = new ZOYPersp();
						break;
					case "ZOX":
						CurPersp = new ZOXPersp();
						break;
				}
				refresh();
			}
		});
	}

	public void loadFile(ActionEvent actionEvent) {
		initTog();
		initOS();
		System.out.println("load");
		File f = null;
		Scanner in = null;
		try {
			f = new File(getClass().getResource("in").toURI());
			in = new Scanner(f);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int max = 0;
		edges = new ArrayList<>();
		while (in.hasNextInt()) {
			int x, y, z;
			x = in.nextInt();
			y = in.nextInt();
			z = in.nextInt();

			int x1, y1, z1;
			x1 = in.nextInt();
			y1 = in.nextInt();
			z1 = in.nextInt();


			edges.add(new Edge(add(x, y, z), add(x1, y1, z1)));
			max = Math.max(max, Math.max(Math.max(x, Math.max(y, z)), Math.max(x1, Math.max(y1, z1))));
		}
		scale = 8 * con * max;
		intitCOf();
		refresh();
	}


	public Point add(int x, int y, int z) {
		Point t = new Point(x, y, z);
		if (pointSet.contains(t))
			return pointSet.get(pointSet.indexOf(t));
		else
			pointSet.add(t);
		return t;
	}


	public void PressKey(KeyEvent keyEvent) {
		double cost = 0.1;
		int icost = 1;

		if (keyEvent.isShiftDown()) {
			cost = -cost;
			icost = -icost;
		}


		if (keyEvent.isControlDown()) {
			if (keyEvent.getCode() == KeyCode.X) {
				translateX += cost;
			} else if (keyEvent.getCode() == KeyCode.C) {
				translateY += cost;
			} else if (keyEvent.getCode() == KeyCode.Z) {
				translateZ += cost;
			}
		} else if (keyEvent.isAltDown()) {
			if (keyEvent.getCode() == KeyCode.X) {
				scaleX += cost;
			} else if (keyEvent.getCode() == KeyCode.C) {
				scaleY += cost;
			} else if (keyEvent.getCode() == KeyCode.Z) {
				scaleZ += cost;
			}
		} else {
			if (keyEvent.getCode() == KeyCode.X) {
				phi += icost;
			} else if (keyEvent.getCode() == KeyCode.C) {
				psi += icost;
			} else if (keyEvent.getCode() == KeyCode.Z) {
				hi += icost;
			} else if (keyEvent.getCode() == KeyCode.A) {
				px += 10000 * cost;
			} else if (keyEvent.getCode() == KeyCode.S) {
				py += 10000 * cost;
			} else if (keyEvent.getCode() == KeyCode.D) {
				pz += 10000 * cost;
			}
		}

		System.out.println("px" + px);
		System.out.println("py" + py);
		System.out.println("pz" + pz);
		refresh();
	}

}
