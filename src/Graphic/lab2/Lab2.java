package Graphic.lab2;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;

import java.io.IOException;

public class Lab2 extends Application {
    private WritableImage dest;
    private int width;
    private int height;


    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("lab2.fxml"));
        Scene scene = new Scene(root);

        stage.setTitle("Lab1");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}